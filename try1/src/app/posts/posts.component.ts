import { Comment } from './../interfaces/comment';
import { Observable } from 'rxjs';
import { AuthService } from './../auth.service';
import { PostService } from './../service/post.service';
import { Post } from './../interfaces/post';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
//  posts$:Observable<Post>
  comments$;

  comments:Comment[];
posts$;
post:Post[];
userId:string;
postId:String;
panelOpenState = false;
items=[];
message :"Saved for later viewing";

  constructor(private postService:PostService,public authService:AuthService) { }


savepost(id:string,title:string,body:string){
  this.postService.savepost(this.userId,id,title,body)
  this.items.push(id);
  this.postId = id;


}





  ngOnInit(): void {

    //this.posts$ = this.postService.getPosts(); 


    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;

           }
        )

    this.postService.getCom().subscribe(data =>this.comments$ = data );

    this.postService.getPosts().subscribe(data =>this.posts$ = data );   

    
  }

}
