import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/internal/operators/map';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  customersCollection:AngularFirestoreCollection;  
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 
  

  public getAllcusNew(userId): Observable<any[]> {
    this.customersCollection = this.db.collection(`users/${userId}/customers`);
    return this.customersCollection.snapshotChanges()
     
  }

  


  public getAllCus(userId){
    this.customersCollection = this.db.collection(`users/${userId}/customer`);
    return this.customersCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data(); 
          data.id = document.payload.doc.id;
          return data; 
        }
      )
    ))
      }
      

 

    deleteCus(userId:string, id:string){
      this.db.doc(`users/${userId}/customers/${id}`).delete();
    }

    addcus(userId:string,id:string,name:string,education:number,income:number){
      const customer = { name:name,
        education:education,
        income:income}; 
      this.userCollection.doc(userId).collection('customers').add(customer);
    }
     
  
    updateCustomer(userId:string,id:string,name:string,education:number,income:number){
      this.db.doc(`users/${userId}/customers/${id}`).update(
        {
          name:name,
          education:education,
          income:income,
          result:null,
        
          
        }
      )
    }

///////////////////



private url =  "https://h12qhr9xle.execute-api.us-east-1.amazonaws.com/beta";
  

classify(userId:string,id:string,education:number,income:number){

  let json = {'data':
    {'education':education,
    'income':income}
  }
  console.log(json);
  let body = JSON.stringify(json);
  console.log(body)
     return this.http.post<any>(this.url,body).pipe(
    map(res => {
    console.log(res);
    var final = res.body;
    
    console.log(final);

    return final;

  }
  )
  )
      

}

updatePredict(userId:string,id:string,result:string,saved:Boolean){
console.log(id,result)
  this.db.doc(`users/${userId}/customers/${id}`).update(
    {
      result:result,
      saved:saved
     
    }
  )


}




  constructor(private db:AngularFirestore,private http:HttpClient) { }
}
