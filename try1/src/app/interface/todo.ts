export interface Todo {
    id:string,
    title:string,
    info:string,
    type:string,
    duedate?:Date 
    done?:boolean  

}
