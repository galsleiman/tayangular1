import { Observable } from 'rxjs';
import { Task } from './../interfaces/task';
import { AuthService } from './../auth.service';
import { TasksService } from './../tasks.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-done',
  templateUrl: './done.component.html',
  styleUrls: ['./done.component.css']
})
export class DoneComponent implements OnInit {
 
  dones$:Observable<any>;
  dones;

 // tasks:Task[];
  userId:string;
  panelOpenState = false;
  
    constructor(private tasksService:TasksService,public authService:AuthService) { }


  
  
    ngOnInit(): void {
      this.authService.getUser().subscribe(
        user => {
          this.userId = user.uid;
          console.log(this.userId);
          this.dones$ = this.tasksService.getAllDone(this.userId);
        }
          ) 
  
  
        }
    
    }
  
   // ngOnInit(): void {
      
     // this.tasks$=this.tasksService.getAll();
      //this.tasks$.subscribe(tasks=>this.tasks=tasks);
    //}
  
