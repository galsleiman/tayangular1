import { UsersComponent } from './users/users.component';
import { SavepostsComponent } from './saveposts/saveposts.component';
import { PostsComponent } from './posts/posts.component';
import { CustomersComponent } from './customers/customers.component';
import { DoneComponent } from './done/done.component';
import { LoginComponent } from './login/login.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { TasksComponent } from './tasks/tasks.component';
import { MyusersComponent } from './myusers/myusers.component';
import { TodosComponent } from './todos/todos.component';




const routes: Routes = [



  { path: 'signup', component: SignupComponent},
  { path: 'login', component: LoginComponent},
  { path: 'welcome', component: WelcomeComponent},
  { path: 'tasks', component: TasksComponent },
  { path: 'done', component: DoneComponent},
  { path: 'customers', component:CustomersComponent},
  { path: 'posts', component:PostsComponent},
  { path: 'saveposts', component:SavepostsComponent},
  { path: 'allusres', component: UsersComponent},
  { path: 'myusres', component: MyusersComponent},
  { path: 'todos', component: TodosComponent},
];





@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }