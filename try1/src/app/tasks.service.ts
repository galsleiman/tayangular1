
import { stringify } from '@angular/compiler/src/util';
import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { observable, Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';



@Injectable({
  providedIn: 'root'
})
export class TasksService {

  doneCollection:AngularFirestoreCollection;  
  taskCollection:AngularFirestoreCollection;  
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 
  

  //tasks = [{title:'angular', duedate:'27/1/2021', info:"leren leren leren",type:'study'},
 // {title:'power-bi', duedate:'24/2/2021', info:"finish first scope",type:'adi'},
 /// {title:'axure', duedate:'2/2/2021', info:"routes and fun",type:'study'}];

 // getAll(){
  //  return this.tasks;
 // }

////1!
 // public getAll(){
   // const tasksObservable= new Observable(observer=> { 
     // setInterval(()=>observer.next(this.tasks),500)
    //});
    ///return tasksObservable;
       // }
      

        public getAll(userId){
          this.taskCollection = this.db.collection(`users/${userId}/tasks`);
          return this.taskCollection.snapshotChanges()
           
        }


     


        public getAllDone(userId){
          this.doneCollection = this.db.collection(`users/${userId}/dones`);
          return this.doneCollection.snapshotChanges().pipe(map(
            collection =>collection.map(
              document => {
                const data = document.payload.doc.data(); 
                data.id = document.payload.doc.id;
                return data; 
              }
            )
          ))
          
           
        }
        

        public deleteTask(userId:string , id:string){
          this.db.doc('users/${userId}/tasks/${id}').delete();
        }

        addtask(userId:string,id:string,title:string,info:string,type:string,duedate:Date){
          const task = { title:title,
            info:info,
            type:type,
            duedate:duedate}; 
          this.userCollection.doc(userId).collection('tasks').add(task);
        }
      
 updateTask(userId:string,id:string,title:string,info:string,type:string,duedate:Date){

  duedate.setHours(0)
  duedate.setMinutes(0)
  duedate.setSeconds(0)
  duedate.setMilliseconds(0)
  duedate.getTime();
  console.log(duedate);



 this.db.doc(`users/${userId}/tasks/${id}`).update(
      {
        title:title,
        info:info,
        type:type,
        duedate:duedate
        
      }
    )
  }
  

  
  doneTask(userId:string,id:string,title:string,info:string,type:string,duedate:Date ){
    const done = {
      title:title,
      info:info,
      type:type,
      duedate:duedate 
    }

    this.userCollection.doc(userId).collection('dones').add(done);

  }

        constructor(private db:AngularFirestore ) { }
      




}
