import { Observable } from 'rxjs';
import { AuthService } from './../auth.service';
import { UsersbService } from './../service/usersb.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-myusers',
  templateUrl: './myusers.component.html',
  styleUrls: ['./myusers.component.css']
})
export class MyusersComponent implements OnInit {



  mys$:Observable<any>;
  mys;

 // tasks:Task[];
  userId:string;
  panelOpenState = false;
  
    constructor(private userbService:UsersbService,public authService:AuthService) { }


  
  
    ngOnInit(): void {
      this.authService.getUser().subscribe(
        user => {
          this.userId = user.uid;
          console.log(this.userId);
          this.mys$ = this.userbService.getAllMY(this.userId);
        }
          ) 
  
  
        }
    
    }