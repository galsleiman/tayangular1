import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { UsersbRow } from './../interface/usersb-row';
import { Usersb } from './../interfaces/usersb';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class UsersbService {
  
  myusersCollection:AngularFirestoreCollection;  
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 
  errorMessage:string; 
  isError:boolean = false; 

  private UsersApi = "https://jsonplaceholder.typicode.com/users" ;

//Usersb:Observable<UsersbRow>;



  getUsers()
  {
       return this.http.get<Usersb>(`${this.UsersApi}`);

       //.pipe(
       // map(data => this.transformData(data)));
  }

  getAllMY(userId){
    this.myusersCollection = this.db.collection(`users/${userId}/myusers`);
    return this.myusersCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data(); 
          data.id = document.payload.doc.id;
          return data; 
        }
      )
    ))
      }

      getAllMY1(userId){
        this.myusersCollection = this.db.collection(`users/${userId}/myusers`);
        return this.myusersCollection.snapshotChanges().pipe(map(
          collection =>collection.map(
            document => {
              const data = document.payload.doc.data(); 
              data.id = document.payload.doc.id;
              return data; 
            }
          )
        ))
        
         
      }




 adduser
  (userId:string,id:string,email:string,name:string){
    const myuser = { email:email,
      name:name,
      id:id}; 
    this.userCollection.doc(userId).collection('myusers').add(myuser);
    let password="12345678";
    this.authService.SignUp(email,password).then(res => {
      console.log(res);
      this.router.navigate(['/again']); 
    }).catch(
      err => {
        console.log(err);
        this.isError = true; 
        this.errorMessage = err.message; 
      } 
    ) 
  }



  


  //private transformData(data:UsersbRow):Usersb{  
  //  console.log('Usersb::',data);
  //  return {
    //  id:data.id,
      //name:data.name,
      //email:data.email,
      //phone: data.phone,
      //website:data.website,
      ///city:data.address[0].city,

    
      
    //}
 // }

  
  constructor(private http: HttpClient ,private db: AngularFirestore,public authService:AuthService,private router:Router) { }
}
