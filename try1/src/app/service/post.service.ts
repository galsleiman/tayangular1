import { Comment } from './../interfaces/comment';
import { Observable } from 'rxjs';
import { Post } from './../interfaces/post';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  private PostApi = "https://jsonplaceholder.typicode.com/posts" ;
  private ComAPI="http://jsonplaceholder.typicode.com/comments";

  postsCollection:AngularFirestoreCollection;  
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 



//   getPosts(): Observable<Post>
// {
    // return this.http.get<Post>(`${this.PostApi}`);
  //}



  getCom()
  {
      return this.http.get<Comment>(`${this.ComAPI}`);
   }

   getPosts()
{
     return this.http.get<Post>(`${this.PostApi}`);
}

savepost
  (userId:string,id:string,title:string,body:string){
    const post = { title:title,
      body:body,
      id:id}; 
    this.userCollection.doc(userId).collection('posts').add(post);
  }

  getAllPOST(userId){
    this.postsCollection = this.db.collection(`users/${userId}/posts`);
    return this.postsCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data(); 
          data.id = document.payload.doc.id;
          return data; 
        }
      )
    ))
    
     
  }
  


constructor(private http: HttpClient ,private db: AngularFirestore) { }


 

}
