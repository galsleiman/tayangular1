import { TestBed } from '@angular/core/testing';

import { UsersbService } from './usersb.service';

describe('UsersbService', () => {
  let service: UsersbService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UsersbService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
