import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TodosService {

  todoCollection:AngularFirestoreCollection;  
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 

  addtask(userId:string,id:string,title:string,info:string,type:string,duedate:Date){
    const todo = { title:title,
      info:info,
      type:type,
      duedate:duedate}; 
    this.userCollection.doc(userId).collection('todo').add(todo);
  }
  
  public getAll(userId){
    this.todoCollection = this.db.collection(`users/${userId}/todo`);
    return this.todoCollection.snapshotChanges()
     
  }

  updatePredict(userId:string,id:string,done:Boolean){
  
      this.db.doc(`users/${userId}/todo/${id}`).update(
        {
         done:done
         
        }
      )
      }
  

  constructor(private db:AngularFirestore) { }
}
