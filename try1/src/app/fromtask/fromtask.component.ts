
import { Task } from './../interfaces/task';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { dateInputsHaveChanged } from '@angular/material/datepicker/datepicker-input-base';

@Component({
  selector: 'fromtask',
  templateUrl: './fromtask.component.html',
  styleUrls: ['./fromtask.component.css']
})
export class FromtaskComponent implements OnInit {
  types:Object[] = [{name:'adi'},{name:'study'},{name:'work'},{name:'fun'}]

  @Input() title:string;  
  @Input() info:string; 
  @Input() type:string;
  @Input() duedate:Date;
  @Input() id:string;
  @Input() formType:string;
  @Output() update = new EventEmitter<Task>();
  @Output() closeEdit = new EventEmitter<null>();







  updateParent(){
    
  const duedate=new Date(this.duedate)
    console.log(duedate);


    let task:Task = {id:this.id, title:this.title, info:this.info,type:this.type,duedate:this.duedate};
    this.update.emit(task); 

    if(this.formType == "Add task"){
      this.title  = null;
      this.info = null; 
      this.type  = null;
      this.duedate = null; 
    }
  }

  tellParentToClose(){
    this.closeEdit.emit(); 
  }

  constructor() { }

  ngOnInit(): void {
  }

}
