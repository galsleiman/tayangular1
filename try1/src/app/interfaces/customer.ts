export interface Customer {


    id?:string,
    name: string;
    income: number;
    education: number;
    result?:string;
    saved?:Boolean;
   
}
