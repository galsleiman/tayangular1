

export interface Task {

        id:string,
        title:string,
        info:string,
        type:string,
        duedate?:Date      
    
}
