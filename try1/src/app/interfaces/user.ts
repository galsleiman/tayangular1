export interface User {
    uid:string,
    email?: string | null,
    age?: number| null,
    photoUrl?: string,
    displayName?:string        
}

