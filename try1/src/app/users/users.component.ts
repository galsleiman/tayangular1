import { Observable } from 'rxjs';
import { AuthService } from './../auth.service';
import { UsersbService } from './../service/usersb.service';
import { Usersb } from './../interfaces/usersb';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
usersb$;
panelOpenState = false;
usersb:Usersb[];
items=[];
userId;

mys:Usersb[];
mys$;


constructor(private usrsbService:UsersbService,public authService:AuthService) { }

adduser(id:string,email:string,name:string){
 
  this.usrsbService.adduser(this.userId,id,email,name);
 
  this.items.push(id);

}



  ngOnInit(): void {

    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;

           }
        )



    this.usrsbService.getUsers().subscribe(data =>this.usersb$ = data );  

    this.usrsbService.getAllMY1(this.userId).subscribe(data =>this.mys$ = data );


  }

}
