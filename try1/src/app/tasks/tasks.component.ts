import { ClassifyService } from './../classify.service';
import { DoneComponent } from './../done/done.component';
import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';
import { TasksService } from './../tasks.service';
import { Task } from './../interfaces/task';

@Component({
  selector: 'tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})

export class TasksComponent implements OnInit {
//tasks;
tasks$;
tasks:Task[];
userId:string;
taskId:string;
editstate= [];
addTaskFormOpen = false;
panelOpenState = false;
clicktoprobility=false;

probility;



  constructor(private tasksService:TasksService,public authService:AuthService,private classifyService:ClassifyService) { }

deleteTasks(id:string){
    this.tasksService.deleteTask(this.userId,id);
  }

  update(task:Task){
  const duedate=new Date(task.duedate)
   duedate.setHours(0);
   duedate.setMinutes(0);
   duedate.setSeconds(0);
   duedate.getTime();
  console.log(duedate);

    this.tasksService.updateTask(this.userId, task.id ,task.title, task.info,task.type,duedate);
  }

  move(id:string,title:string,type:string,duedate:Date,info:string){
    this.taskId = id;
    console.log(duedate);
    this.tasksService.doneTask(this.userId, id ,title,info,type,duedate);
  }

  add(task:Task){

    const duedate=new Date(task.duedate)
    duedate.setHours(0);
    duedate.setMinutes(0);
    duedate.setSeconds(0);
    duedate.getTime();
    this.tasksService.addtask(this.userId, task.id ,task.title, task.info,task.type,duedate); 
  }
  

  classify(id:string,type:string,duedate:Date){
    this.classifyService.classify(this.userId,id,type,duedate).subscribe(
      res => {
       console.log(res);
       this.probility = res;
        
      }
    )
  }
  






  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
        this.tasks$ = this.tasksService.getAll(this.userId);

        this.tasks$.subscribe(
          docs =>{
            this.tasks = [];
            for(let document of docs){
              const task:Task = document.payload.doc.data();
              task.id = document.payload.doc.id; 
              this.tasks.push(task); 
            }
          }
        ) 


      }
    )
  }


  
 // ngOnInit(): void {
    
   // this.tasks$=this.tasksService.getAll();
    //this.tasks$.subscribe(tasks=>this.tasks=tasks);
  //}

}
