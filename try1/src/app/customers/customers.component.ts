import { CustomerService } from './../customer.service';
import { AuthService } from './../auth.service';
import { Customer } from './../interfaces/customer';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Component, OnInit, ViewChild } from '@angular/core';
//import {AfterViewInit, Component, ViewChild} from '@angular/core';



@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']

})

export class CustomersComponent implements OnInit {


  addTaskFormOpen:false;
userId:string;
  displayedColumns: string[] = ['name', 'Education in years', 'Personal income','Delete', 'Edit','Predict','Result','Save'];
  
  customers:Customer[];
  customers$;
  dataSource;
  editRow:Customer = {name:null, education:null, income:null};
  rowToEdit:number=null;
  probility;
  predictState =[];
 

 // dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  

  add(customer:Customer){
    this.customersService.addcus(this.userId, customer.id ,customer.name, customer.education,customer.income); 
  }
  
  deletecus(index){
    let id = this.customers[index].id;
    console.log()
    this.customersService.deleteCus(this.userId,id);
  }

 

  moveToEditState(index){
    
    this.editRow.name = this.customers[index].name;
    this.editRow.education = this.customers[index].education;
    this.editRow.income = this.customers[index].income;
    this.rowToEdit = index; 
  }

  updateCustomer(){
    let id = this.customers[this.rowToEdit].id;
    this.customersService.updateCustomer(this.userId,id, this.editRow.name,this.editRow.education ,this.editRow.income);
    this.rowToEdit = null;
  }

 
  

  classify(index){
    this.customersService.classify(this.userId,this.customers[index].id,this.customers[index].education, this.customers[index].income).subscribe(
      res => {
        console.log(res);
    console.log(typeof(res))
        if (res>0.5){
          res='Will return'
        }else{ res ='dont Will return!!!!!!'}
    this.customers[index].result = res;
      }
    )
    }

      
  save(index){
    console.log(this.userId,this.customers[index].id,this.customers[index].result)
    this.customers[index].saved=true;
    this.customersService.updatePredict(this.userId,this.customers[index].id,this.customers[index].result,this.customers[index].saved);
  }

    
  


  ngOnInit() {

    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId); 
        this.customers$ = this.customersService.getAllcusNew(this.userId);  
        this.customers$.subscribe(
          docs =>{
            this.customers = [];
            
            for(let document of docs){
              const customer:Customer = document.payload.doc.data();
              customer.id = document.payload.doc.id; 
              this.customers.push(customer); 
      
            }
            this.dataSource = new MatTableDataSource<Customer>(this.customers);
            
            this.dataSource.paginator = this.paginator;
          }
          
        ) 


      }
    )

  
  }


  ngAfterViewInit() {
    this.dataSource = new MatTableDataSource([this.customers]);
    this.dataSource.paginator = this.paginator;
 
  }

  constructor(private customersService:CustomerService,public authService:AuthService) { }

}






