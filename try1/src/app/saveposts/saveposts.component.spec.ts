import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SavepostsComponent } from './saveposts.component';

describe('SavepostsComponent', () => {
  let component: SavepostsComponent;
  let fixture: ComponentFixture<SavepostsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SavepostsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SavepostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
