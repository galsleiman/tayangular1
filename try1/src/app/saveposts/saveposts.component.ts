import { Observable } from 'rxjs';
import { AuthService } from './../auth.service';
import { PostService } from './../service/post.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-saveposts',
  templateUrl: './saveposts.component.html',
  styleUrls: ['./saveposts.component.css']
})
export class SavepostsComponent implements OnInit {

  Sposts$:Observable<any>;
  Spost;

 // tasks:Task[];
  userId:string;
  panelOpenState = false;
  
    constructor(private postService:PostService,public authService:AuthService) { }


  
  
    ngOnInit(): void {
      this.authService.getUser().subscribe(
        user => {
          this.userId = user.uid;
          console.log(this.userId);
          this.Sposts$ = this.postService.getAllPOST(this.userId);
        }
          ) 
  
  
        }
    
    }