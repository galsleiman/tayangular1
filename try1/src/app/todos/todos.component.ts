import { UsersbService } from './../service/usersb.service';
import { Observable } from 'rxjs';
import { AuthService } from './../auth.service';
import { TodosService } from './../service/todos.service';
import { Todo } from './../interface/todo';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})


export class TodosComponent implements OnInit {
  addTaskFormOpen = false;
  panelOpenState = false;
todos$;
todos:Todo[];
userId:string;
mys$:Observable<any>;
  mys;

  
  add(todo:Todo){

    const duedate=new Date(todo.duedate)
    duedate.setHours(0);
    duedate.setMinutes(0);
    duedate.setSeconds(0);
    duedate.getTime();
    this.todoService.addtask(this.userId, todo.id ,todo.title, todo.info,todo.type,duedate); 
  }
  
  save(id:string,done:boolean){

    done=true;
    this.todoService.updatePredict(this.userId,id,done);
  }

    
  

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
        this.todos$ = this.todoService.getAll(this.userId);

        this.todos$.subscribe(
          docs =>{
            this.todos = [];
            for(let document of docs){
              const todo:Todo = document.payload.doc.data();
              todo.id = document.payload.doc.id; 
              this.todos.push(todo); 
            }
          }
        ) 


      }
    )

     


  }

  constructor(private todoService:TodosService,public authService:AuthService,private userbService:UsersbService) { }
}