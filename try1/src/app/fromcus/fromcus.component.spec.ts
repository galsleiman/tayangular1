import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FromcusComponent } from './fromcus.component';

describe('FromcusComponent', () => {
  let component: FromcusComponent;
  let fixture: ComponentFixture<FromcusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FromcusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FromcusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
