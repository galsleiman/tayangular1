import { Customer } from './../interfaces/customer';
import {FormControl, Validators} from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'fromcus',
  templateUrl: './fromcus.component.html',
  styleUrls: ['./fromcus.component.css']
})
export class FromcusComponent implements OnInit {

  isError:boolean = false;
  errorMessage:string; 

  Error:string;
  Error2:string;
  
  Error1:string;
  @Input() name:string;  
  @Input() education:number; 
  @Input() income:number
  @Input() id:string;
  @Output() update = new EventEmitter<Customer>();
  @Output() closeEdit = new EventEmitter<null>();


 
  isFormValid(){


    if(this.education>24|| this.education<0){
      this.Error = "Invalid value, please have a your income "
      return false;
    }
    return true;
  }

  clearError(){
    this.Error = '';
  }


  updateParent(){
    if(this.isFormValid()){
    let customer:Customer = {id:this.id, name:this.name, education:this.education,income:this.income}
    this.update.emit(customer); 
      this.name  = null;
      this.education = null; 
      this.income  = null;
      this.closeEdit.emit();
   
    }}


  tellParentToClose(){
    this.closeEdit.emit(); 
  }


  constructor() { }

  ngOnInit(): void {
  }

}