



import { WelcomeComponent } from './welcome/welcome.component';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';

import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';





import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCardModule} from '@angular/material/card';
import { HttpClientModule } from '@angular/common/http';
import {MatSelectModule} from '@angular/material/select';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { AngularFireAuthModule } from '@angular/fire/auth';
import {AngularFirestoreModule} from '@angular/fire/firestore';




import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';

import { TasksComponent } from './tasks/tasks.component';
import { FromtaskComponent } from './fromtask/fromtask.component';
import { DoneComponent } from './done/done.component';
import { CustomersComponent } from './customers/customers.component';
import { FromcusComponent } from './fromcus/fromcus.component';
import { PostsComponent } from './posts/posts.component';
import { SavepostsComponent } from './saveposts/saveposts.component';
import {UsersComponent} from './users/users.component';
import { AgainComponent } from './again/again.component';
import { MyusersComponent } from './myusers/myusers.component';
import { TodosComponent } from './todos/todos.component';



@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    NavComponent,
    LoginComponent,
    SignupComponent,
    TasksComponent,
    FromtaskComponent,
    DoneComponent,
    CustomersComponent,
    FromcusComponent,
    PostsComponent,
    SavepostsComponent,
UsersComponent,
AgainComponent,
MyusersComponent,
TodosComponent,
  
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatInputModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatCardModule,
    MatListModule,
    MatExpansionModule,
    HttpClientModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    
    AngularFireAuthModule,
    AngularFirestoreModule,
    MatDatepickerModule,
    MatTableModule,
    MatPaginatorModule,


    
    

 
    

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }