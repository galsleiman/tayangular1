// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

   firebaseConfig :{
    apiKey: "AIzaSyA-1mCT3gGF9sOt1DhXeWP3SZq7tyH3lio",
    authDomain: "try1-57446.firebaseapp.com",
    databaseURL: "https://try1-57446.firebaseio.com",
    projectId: "try1-57446",
    storageBucket: "try1-57446.appspot.com",
    messagingSenderId: "37908149984",
    appId: "1:37908149984:web:f098762327f52cf6bc0285",
    measurementId: "G-9GVGDZB9EJ"
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
